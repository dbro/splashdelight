import logging
import webapp2
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext import ndb
from google.appengine.api import taskqueue
from google.appengine.api import mail
import datetime
import re
from uuid import uuid4

########################################################################
# RFC822 Email Address Regex
# Originally written by Cal Henderson
# c.f. http://iamcal.com/publish/articles/php/parsing_email/
# Translated to Python by Tim Fletcher, with changes suggested by Dan Kubb.
# Licensed under a Creative Commons Attribution-ShareAlike 2.5 License
# http://creativecommons.org/licenses/by-sa/2.5/
qtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]'
dtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]'
atom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+'
quoted_pair = '\\x5c[\\x00-\\x7f]'
domain_literal = "\\x5b(?:%s|%s)*\\x5d" % (dtext, quoted_pair)
quoted_string = "\\x22(?:%s|%s)*\\x22" % (qtext, quoted_pair)
domain_ref = atom
sub_domain = "(?:%s|%s)" % (domain_ref, domain_literal)
word = "(?:%s|%s)" % (atom, quoted_string)
domain = "%s(?:\\x2e%s)*" % (sub_domain, sub_domain)
local_part = "%s(?:\\x2e%s)*" % (word, word)
addr_spec = "%s\\x40%s" % (local_part, domain)
email_address = re.compile('\A%s\Z' % addr_spec)
def isValidEmailAddress(email):
    if email_address.match(email):
        return True
    else:
        return False
########################################################################

class Person(ndb.Model):
    """Models an individual person"""
    # the entity key is the email = ndb.StringProperty(indexed=True)
    created = ndb.DateTimeProperty(indexed=False, auto_now_add=True)
    email_validated = ndb.BooleanProperty(indexed=True, default=False)
    author_last = ndb.DateTimeProperty(indexed=False) # when the person last sent content to us
    author_count = ndb.IntegerProperty(indexed=False, default=0)
    recipient_last = ndb.DateTimeProperty(indexed=True, auto_now_add=True) # might need a default for the index?
    recipient_count = ndb.IntegerProperty(indexed=False, default=0)
    thanked_last = ndb.DateTimeProperty(indexed=False)
    thanked_count = ndb.IntegerProperty(indexed=False, default=0)
    net_author_count = ndb.IntegerProperty(indexed=False, default=0)
    net_author_count_positive = ndb.BooleanProperty(indexed=True, default=False)

class Message(ndb.Model):
    """a piece of content created by a person which will be sent to another person. used for logging"""
    created = ndb.DateTimeProperty(indexed=False, auto_now_add=True)
    author = ndb.StringProperty(indexed=False)
    content_subject = ndb.StringProperty(indexed=False)
    content_body = ndb.StringProperty(indexed=False)
    recipient = ndb.StringProperty(indexed=False)
    sent = ndb.DateTimeProperty(indexed=False)
    thanked = ndb.DateTimeProperty(indexed=False)
    thank_subject = ndb.StringProperty(indexed=False)
    thank_body = ndb.StringProperty(indexed=False)
    attachment_name = ndb.StringProperty(indexed=False)
    attachment_data = ndb.BlobProperty(indexed=False)

class ReceiveHandler(InboundMailHandler):
    def receive(self, mail_message):
        to = mail_message.to
        #logging.info("Received request to: " + to)
        if to[:7] == "thanks_":
            # this is a thanks message
            #logging.info("Queuing thanks request: " + to)
            plaintext_bodies = mail_message.bodies('text/plain')
            fullbody = ""
            for content_type, body in plaintext_bodies:
                fullbody = fullbody + body.decode() + '\n'
            taskqueue.add(url=("/%s" % (to.split('@'))[0]), countdown=1, 
                params={"subject": mail_message.subject, "body":fullbody})
            return # assume there is no new message content
            # TODO: check to see if it's also a new message.
            # should compare with old message to see if it's also a new piece of content
        if "X-Gm-Original-To" in mail_message.original:
            author_email = mail_message.original["X-Gm-Original-To"]
            logging.info("Found original to in message header: " + author_email)
        else:
            author_email = mail_message.sender
        logging.debug("Received a message from: " + author_email)
        # step 1 save the author's email address in the datastore
        author_email_stripped = author_email.split("<")[-1].split(">")[0] # retain stuff between "<" and ">"
        author_email = author_email_stripped
        author = ndb.Key('Person', author_email).get()
        if not author:
            logging.debug("Creating an entry in the database for person: " + author_email)
            author = Person(id = author_email)
            author.email_validated = True # TODO: don't set email_validated to True yet, validate it first
        author.author_last = datetime.datetime.now()
        author.author_count += 1
        author.net_author_count += 1
        author.net_author_count_positive = True if author.net_author_count > 0 else False
        author.put_async()
        # step 2 save the content in the datastore (optional) - this is just for logging purposes
        # TODO: handle html format email too
        plaintext_bodies = mail_message.bodies('text/plain')
        fullbody = ""
        for content_type, body in plaintext_bodies:
            fullbody = fullbody + body.decode() + '\n'
        message_id = uuid4().hex # create this ahead of time to avoid write-read round trip
        message = Message(id=message_id,
            author=author_email,
            content_subject=mail_message.subject,
            content_body=fullbody)
        # handle attachments
        message_params={
            "author_email":author_email,
            "content_subject":mail_message.subject,
            "content_body":fullbody,
            "message_id":message_id,
        }
        if hasattr(mail_message, 'attachments'):
            for filename, filecontents in mail_message.attachments:
                message.attachment_name = filename
                filedata = filecontents.decode()
                message.attachment_data = filedata
                #message_params["attachment_name"] = filename # disabled due to size restriction
                #message_params["attachment_data"] = filedata
                break # only keep one attachment
        message.put_async()
        # step 3 create a task to send the content to someone else
        # wait at least 1 second before executing the task. allows the datastore to catch up
        taskqueue.add(url="/send8675309", countdown=1, params=message_params)

class SendContentToPerson(webapp2.RequestHandler):
    def post(self):
        author_email = self.request.get("author_email")
        content_subject = self.request.get("content_subject")
        content_body = self.request.get("content_body")
        message_id = self.request.get("message_id")
        attachment_name = self.request.get("attachment_name") or None
        attachment_data = self.request.get("attachment_data") or None
        if (attachment_name and attachment_data):
            attachments=[(attachment_name, attachment_data)]
        else:
            attachments=None
        #logging.info("will send this content body : " + content_body)
        # step 1: find person to receive this content
        # should be someone else, and been waiting a long time to receive something
        # and have sent more than they received
        recipient_qry = Person.query().filter(Person.email_validated == True).filter(Person.net_author_count_positive == True).order(Person.recipient_last)
        possible_recipients = recipient_qry.fetch(2)
        recipient = None # default
        recipient_email = "" # default
        logging.info("found %d possible recipients" % len(possible_recipients))
        for r in possible_recipients:
            recipient = r
            recipient_email = recipient.key.id() # the key is the email
            if recipient_email != author_email:
                break
        if recipient_email == author_email:
            logging.info("no valid recipients found. author and recipient cannot be the same")
            self.error(404)
            return

        # step 2a: postpone this task if no appropriate recipient exists
        if ((recipient is None) or (recipient_email == "")):
            logging.info("no valid recipients found")
            self.error(404)
            return

        if not isValidEmailAddress(recipient_email):
            logging.info("invalid email address : %s" % recipient_email)
            # mark the email as invalid in the datastore
            recipient.email_validated=False
            recipient.put_async()
            # return an error. try again later
            self.error(404)
            return
        else:
            # step 2b: send the content
            sender_address = "Splash Delight <thanks_%s@splashdelight.appspotmail.com>" % message_id
            subject = "Splash for you"
            body = """
This is something delightful, we hope:
%s
%s

---
You can say "Thanks" by replying to this message or visiting http://www.splashdelight.com/thanks_%s
""" % (content_subject, content_body, message_id)
            # TODO: create the html version of the body, with links for thanks
            message_sent = datetime.datetime.now()
            if attachments:
                mail.send_mail(sender_address, recipient_email, subject, body, attachments=attachments)
            else:
                mail.send_mail(sender_address, recipient_email, subject, body)
            # step 3: mark the content as "sent" (not necessary, just for logging)
            message = ndb.Key('Message', message_id).get()
            if message:
                logging.debug("Updating message with sent info")
                message.recipient = recipient_email
                message.sent = message_sent
                message.put_async()
            # step 4: update the recipient's last content received time
            if recipient:
                logging.debug("Updating last_recipient for person: " + recipient_email)
                recipient.recipient_last = message_sent
                recipient.recipient_count += 1
                recipient.net_author_count -= 1
                recipient.net_author_count_positive = True if recipient.net_author_count > 0 else False
                recipient.put_async()
        #logging.info("done with SendContentToPerson post() method")

class ThanksForMessage(webapp2.RequestHandler):
    def post(self):
        path = self.request.path
        #logging.info("Received request for path : %s" % path)
        if path[:8] == "/thanks_":
            message_id = path[8:] # TODO: check that it's the proper length?
            message = ndb.Key('Message', message_id).get()
            if message:
                #logging.info("pre-existing value of thanked = %s" % message.thanked)
                if not message.thanked:
                    logging.debug("Updating message %s with thank info" % message_id)
                    thank_time = datetime.datetime.now()
                    message.thanked = thank_time
                    thank_subject = self.request.get("subject")
                    if thank_subject:
                        message.thank_subject = thank_subject
                    thank_body = self.request.get("body")
                    if thank_body:
                        message.thank_body = thank_body
                    message.thank_body = self.request.get("body") or None
                    message.put_async()
                    # TODO: relay the thanks to the original author
                    thanks_location_string = ""
                    if "X-AppEngine-Country" in self.request.headers:
                        thanks_country = self.request.headers["X-AppEngine-Country"]
                        thanks_location_string = thanks_country
                    if "X-AppEngine-City" in self.request.headers:
                        thanks_city = self.request.headers["X-AppEngine-City"]
                        thanks_location_string = thanks_city + ", " + thanks_location_string
                    if thanks_location_string != "":
                        thanks_location_string = " in " + thanks_location_string
                    author_email = message.author
                    author = ndb.Key('Person', author_email).get()
                    if author:
                        logging.debug("Sending thanks message to author %s for message %s" % (author_email, message_id))
                        sender_address = "Splash Delight <inbox@splashdelight.appspotmail.com>"
                        subject = "Thanks for your Splash"
                        body = """
You delighted someone%s! Thanks for making another delightful Splash. You've been thanked %d times.

---
You can delight someone new by replying to this message

http://www.splashdelight.com/
""" % (thanks_location_string, author.thanked_count + 1)
                        mail.send_mail(sender_address, author_email, subject, body)
                        author.thanked_last = thank_time
                        author.thanked_count += 1
                        author.put_async()
                else:
                    logging.debug("Message %s has already been thanked. not updating" % message_id)
            else:
                logging.info("Could not find message %s to thank" % message_id)
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('"The essence of all beautiful art, all great art, is gratitude." - Friedrich Nietzsche')

    def get(self):
        self.post()
        

app = ndb.toplevel(webapp2.WSGIApplication([
    ('/send8675309', SendContentToPerson),
    ('/thanks_.+', ThanksForMessage),
    ('/.+', ReceiveHandler),
], debug=True))

